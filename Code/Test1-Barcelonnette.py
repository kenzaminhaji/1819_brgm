
# coding: utf-8

# In[2]:


#import sys
#!{sys.executable} -m pip install afinn

#import sys
#!{sys.executable} -m pip install textblob



#import sys
#!{sys.executable} -m pip install nltk

import sys
get_ipython().system('{sys.executable} -m pip install textblob-fr')


# In[1]:


import afinn
import pandas as pd 


# In[13]:


from afinn import Afinn
afinn = Afinn(language='fr')
afinn.score('Super !')

# compute sentiment scores (polarity) and labels
#sentiment_scores = [af.score(article) for article in corpus]
#sentiment_category = ['positive' if score > 0 
#                          else 'negative' if score < 0 
#                              else 'neutral' 
#                                  for score in sentiment_scores]
    
    
# sentiment statistics per news category
#df = pd.DataFrame([list(news_df['news_category']), sentiment_scores, sentiment_category]).T
#df.columns = ['news_category', 'sentiment_score', 'sentiment_category']
#df['sentiment_score'] = df.sentiment_score.astype('float')
#df.groupby(by=['news_category']).describe()


# In[4]:


from textblob import Blobber
from textblob_fr import PatternTagger, PatternAnalyzer
tb = Blobber(pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
blob1 = tb(u"Quelle belle matinée")
blob1.sentiment

blob2 = tb(u"C'est une voiture terribles.")
blob2.sentiment



# In[1]:


from textblob import TextBlob

text = '''
J'aime pas les pommes! j'aime les frites
'''

blob = TextBlob(text)
#blob.tags           # [('The', 'DT'), ('titular', 'JJ'),
                    #  ('threat', 'NN'), ('of', 'IN'), ...]

#blob.noun_phrases   # WordList(['titular threat', 'blob',
                    #            'ultimate movie monster',
                    #            'amoeba-like mass', ...])

for sentence in blob.sentences:
    print(sentence.sentiment.polarity)


# In[38]:


from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer
text = '''Quelle belle matinée. Quelle moche matinée.'''
blob = TextBlob(text, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
for sentence in blob.sentences:
    print(sentence.sentiment)


# In[58]:


#Premier test avec Textblob_fr

#Etape 1 : Import des bibliothèques nécessaires

#NLP
from textblob import TextBlob
from textblob_fr import PatternTagger, PatternAnalyzer

#Traitement des données
import pandas as pd




# In[72]:


#Etape 2 Import des données 

df = pd.read_csv("C:/Users/Paul/Desktop/Barcelonnette-export-20180131-080358-corrige.csv",delimiter=';', encoding = "ISO-8859-1")





# In[81]:


#Etape 3 Analyse par Textblob

texts=df["Text"]


Parttexts=texts[:10]
print(len(Parttexts))
for text in Parttexts :
    blob = TextBlob(str(text), pos_tagger=PatternTagger(), analyzer=PatternAnalyzer())
    for sentence in blob.sentences:
        print(sentence)
        print(sentence.sentiment)
        print(1)
    
    


# In[2]:


import nltk
nltk.download()

